import { StatusBar } from 'expo-status-bar';
import {  StyleSheet, Text, View,Image} from 'react-native';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Provider } from 'react-native-paper';
import { theme } from './src/components/core/theme';
import Button from './src/components/Button';
import TextInput from './src/components/TextInput';
import Header from './src/components/Header';
import { Dentistrydoc, StartScreen } from './src/Screens';
import { LoginScreen } from './src/Screens';
import { RegisterScreen } from './src/Screens';
import { ResetPasswordScreen } from './src/Screens';
// import { HomeScreen } from './src/Screens';
import { ScheduleScreen } from './src/Screens';
// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { DoctorsScreen } from './src/Screens';
// import { createDrawerNavigator } from '@react-navigation/drawer';
import { AppointmentConfirmation } from './src/Screens';
import { SelectDoctor } from './src/Screens';
// import DrawerContent from './src/DrawerContent';
import DrawerNavigator from './src/Screens/DrawerNavigator';
import { DateAndTime } from './src/Screens';
import { SelectDate } from './src/Screens';
import DateTime from './src/Screens/DateTime';
import AppointmentList from './src/Screens/AppointmentList';
import {FontAwesome5} from '@expo/vector-icons'
import { DeleteScreen } from './src/Screens';

const Stack = createStackNavigator();
// const Tab = createBottomTabNavigator();
// const Drawer = createDrawerNavigator();

export default function App() {
  return (
   <Provider theme={theme} >
     <NavigationContainer >
       <Stack.Navigator
        initialRouteName='StartScreen'
        screenOptions={{headerShown:false}}
       > 
         <Stack.Screen name='StartScreen' component={StartScreen}/>
         <Stack.Screen name="LoginScreen" component={LoginScreen}/>
         <Stack.Screen name='RegisterScreen' component={RegisterScreen}/>
         <Stack.Screen name='ResetPasswordScreen' component={ResetPasswordScreen}/>
         <Stack.Screen name='HomeScreen' component={DrawerNavigator}/>
         <Stack.Screen name='ProfileScreen' component={ScheduleScreen}/>
         <Stack.Screen name='DoctorsScreen' component={DoctorsScreen}/>
         <Stack.Screen name='AppointmentConfirmation' component={AppointmentConfirmation}/>
         <Stack.Screen name='SelectDoctor' component={SelectDoctor}/>
         <Stack.Screen name='DateAndTime' component={DateAndTime}/>
         <Stack.Screen name='SelectDate' component={SelectDate}/>
         <Stack.Screen name='Dentistrydoc' component={Dentistrydoc}/>
         <Stack.Screen name='AppointmentList' component={AppointmentList}/>
         <Stack.Screen name='DeleteScreen' component={DeleteScreen}/>
         
       </Stack.Navigator>
     </NavigationContainer>
   </Provider>
  );
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});



































































































































































































// ***************

// // import React, { useEffect, useState } from 'react';
// // import { StyleSheet } from 'react-native';
// // import {
// //   View,
// //   Text,
// //   ScrollView,
// //   FlatList, TouchableOpacity,TouchableWithoutFeedback,
// // } from 'react-native';
// // import { DataTable, Card, List, Button, useTheme } from 'react-native-paper';
// // import { useIsFocused } from '@react-navigation/native';
// // import style from 'react-native-datepicker/style';
// // // import styles from '../styles'; 

// // const App = () => {
// //   const [isLoading, setLoading] = useState(false);
// //   const [users, setUsers] = useState([]);
// //   //Load data from API
// //   const getUsers = () => {
// //     fetch("https://doctorapplicaton.herokuapp.com/api/department/?format=json",
    
// //     {
// //       method: 'GET',
// //       headers: {
// //         accept: 'application/json',
// //       },
// //     })
// //       .then((response) => response.json())
// //       .then((json) => {
// //         setUsers(json)
// //         })
// //       .catch((error) => console.error(error))
// //       .finally(() => {
// //         setLoading(false)

// //         }
// //       );      
// //   }
// //   useEffect(() => {
// //       setLoading(true);
// //       getUsers();
// //   }, []);

// //   return (
  
// //       <Card>
// //         <View>
          
        
        
// //         <FlatList
// //                 style={styles.fList}
// //                 data={users}
// //                 keyExtractor={({dept_id}) => dept_id}
// //                 renderItem={({ item }) =>(
// //                   <List.Item
// //                   style={styles.itemList}
// //                     title={item.dept_name}
                
                   
// //                   />
// //                 )}
// //               />

      

// {/* onPress={() => navigate("Item Details",{item: item,getUsers:getUsers}) */}
      
//         {/* <View style={{ padding: 20 }}>
//             {isLoading ? <Text>Loading...</Text> :
//             (
//               <FlatList
//                 data={users}
//                 keyExtractor={({dept_id}) => dept_id}
//                 renderItem={({ item }) =>(
//                   <List.Item
//                     title={item.dept_name}
//                     description={(new Date(item.time_log)).toLocaleString("en-GB", {
//                       day: "numeric",
//                       month: "short",
//                       year: "numeric",
//                       hour: "numeric",
//                       minute: "2-digit"
//                     })}
//                     left={props => <List.Icon {...props} icon="clock" />}
//                     onPress={() => navigate("Item Details",{item: item,getUsers:getUsers})}
//                   />
//                 )}
//               />
//             )
//             }
//           <Button icon="camera" mode="contained" onPress={() => navigate("Add Item",{getUsers:getUsers})}>
//             Add Item
//           </Button>
//         </View> */}
//         {/* </View>
//       </Card> */}
   
//   {/* )
// } */}
// // export default App;

// {/* const styles = StyleSheet.create({
//   itemList:{
//     borderWidth:2

//   },

//   fList:{
//     marginTop:50
//   }
// })
//  */}

// *************


// import React, { useEffect, useState } from 'react';
// import {
//   View,
//   Text,
//   ScrollView,
//   FlatList, TouchableOpacity,TouchableWithoutFeedback,
// } from 'react-native';
// import { DataTable, Card, List, Button, useTheme } from 'react-native-paper';
// import { useIsFocused } from '@react-navigation/native';


// const Home = () => {
//   const [isLoading, setLoading] = useState(false);
//   const [users, setUsers] = useState([]);
//   //Load data from API
//   const getUsers = () => {
//     fetch("https://doctorapplicaton.herokuapp.com/api/department/?format=json",
//     {
//       method: 'POST',
//       headers: {
//         accept: 'application/json',
//         'Content-Type':'application/json'
//       },
//       body:JSON.stringify({
//         dept_id : 3,
//         dept_name: 'one'
//       })
//     })
//       .then((response) => response.json())
//       .then((json) => {
//         setUsers(json)
//         })
//       .catch((error) => console.error(error))
//       .finally(() => {
//         setLoading(false)
//         console.log("Hello")
//         }
//       );      
//       }
//   useEffect(() => {
//       setLoading(true);
//       getUsers();
//   }, []);

//   return (
    
//       <Card>
//         <View >
//             {isLoading ? <Text>Loading...</Text> :
//             (
//               <FlatList
//                 data={users}
//                 keyExtractor={({dept_id}) => dept_id}
//                 renderItem={({ item }) =>(
//                   <List.Item
//                     title={item.dept_name}
//                   />
//                 )}
//               />
//             )}
        
//         </View>
//       </Card>
   
//   )
// }
// export default Home;


// *********************************



// import React, { useEffect, useState } from 'react';
// import {
//   View,
//   ScrollView,
//   FlatList, TouchableOpacity,TouchableWithoutFeedback,
// } from 'react-native';
// import { DataTable, Card, TextInput,  Text,Headline, Subheading , Button, useTheme } from 'react-native-paper';


// const App = ({route, navigation}) => {
//   var {getUsers} = route.params;
//   const [text, setText] = React.useState('');
//   const [isLoading, setLoading] = useState(false);
  
//   //Load data from API
  
//   const addUser = () => {
//     fetch(`https://doctorapplicaton.herokuapp.com/api/department/?format=json`,
//     {
//       method: 'POST',
//       headers: { 'Content-Type': 'application/json' },
//       body: JSON.stringify({ text: `${text}` })
//     })
//       .then((response) => response.json())
//       .then((json) => {
    
//         })
//       .catch((error) => console.error(error))
//       .finally(() => {
//         setLoading(false)
//         getUsers()
//         // navigation.goBack()
//         console.log("Hello")
//         }
//       );      
//   }
//   useEffect(() => {
//       setLoading(false);
//   }, []);

//   return (
//     <ScrollView

//     >
//       <Card>
//         <View style={{ padding: 20 }}>
//             {isLoading ? <Text>Loading...</Text> :
//             (
//               <View>
//               <TextInput
//                 value={text}
//                 onChangeText={text => setText(text)}
//                 mode="outlined"
//                 label="Add Department"
//                 placeholder="Type something"
                
//                 right={<TextInput.Affix text="/100" />}
//               />
//                 <Button icon="camera" mode="contained" onPress={() => addUser()}>
//                   Add Item
//                 </Button>
//               </View>
//             )}
//         </View>
//       </Card>
//     </ScrollView>
//   )
// }
// export default App;



