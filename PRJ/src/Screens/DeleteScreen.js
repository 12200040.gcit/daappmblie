import React, { useEffect, useState } from 'react';
import {
  View,
  ScrollView,
  FlatList, TouchableOpacity,TouchableWithoutFeedback,
} from 'react-native';
import { DataTable, Card, TextInput,  Text,Headline, Subheading , Button, useTheme } from 'react-native-paper';
const DeleteAppt=({route, navigation})=> {
    const [isLoading, setLoading] = useState(false);
    var {item, getAppointment} = route.params;
    const deleteUser = () => {
        fetch(`http://doctorapplicaton.herokuapp.com/api/appointment/`,
        {
          method: 'DELETE',
        })
          .then((response) => response.JSON)
          .catch((error) => console.error(error))
          .finally(() => {
            navigation.goBack()
            console.log("the")
            }
          );
      }
      useEffect(() => {
      }, []);
  return (
    <ScrollView
    style={{marginTop:50}}
    >
      <Card>
        <View style={{ padding: 20 }}>
            {isLoading ? <Text>Loading...</Text> :
            (
              <View>
              <Headline>id: {item.app_date}</Headline>
              <Subheading >time: {item.app_time}</Subheading >
                <Button  mode="contained" onPress={() => deleteUser()}>
                  Delete
                </Button>
              </View>
            )}
        </View>
      </Card>
    </ScrollView>
  )
}
export default DeleteAppt