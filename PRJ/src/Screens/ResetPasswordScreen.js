import React from "react"
import {View,StyleSheet,Text} from 'react-native'
import Header from "../components/Header"
import Paragraph from "../components/Paragraph"
import Button from "../components/Button"
import Background from "../components/Background"
import Logo from "../components/Logo"
import TextInput from "../components/TextInput"
import { useState } from "react"
import { emailValidator } from "../components/core/helper/emailValidator"
import { passwordValidator } from "../components/core/helper/passwordValidator"
import BackButton from "../components/BackButton"
import { TouchableOpacity } from "react-native-gesture-handler"
import { theme } from "../components/core/theme"


export default function ResetPasswordScreen({navigation}){
    const onSubmitPressed = () =>{
        const emailError = emailValidator(email.value);
        if(emailError){
            setEmail({...email,error:emailError});
        }
    }

    const [email,setEmail] = useState({value:"",error:""})
    return(
        <Background >
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
            <Header>Reset Password</Header>
            <TextInput 
                label="Email"
                value={email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={(text)=> setEmail({value:text,error:""})}
                description="You will recieve email with password reset link."
                

            />
            <Button 
            mode="contained"
            onPress={onSubmitPressed}
            >Send Instruction</Button>


        </Background>
        
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:"#fff",
        alignItems:"center",
        justifyContent:"center",
        width:"100%",
    },
    row:{
        flexDirection:'row',
        marginTop:4,
    },
    link:{
        fontWeight:'bold',
        color:theme.colors.primary,
    },
})