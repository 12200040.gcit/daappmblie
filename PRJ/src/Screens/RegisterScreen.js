// import React from "react"
// import {View,StyleSheet,Text} from 'react-native'
// import Header from "../components/Header"
// import Paragraph from "../components/Paragraph"
// import Button from "../components/Button"
// import Background from "../components/Background"
// import Logo from "../components/Logo"
// import TextInput from "../components/TextInput"
// import { useState } from "react"
// import { emailValidator } from "../components/core/helper/emailValidator"
// import { passwordValidator } from "../components/core/helper/passwordValidator"
// import { nameValidator } from "../components/core/helper/nameValidator"
// import BackButton from "../components/BackButton"
// import { theme } from "../components/core/theme"
// import { TouchableOpacity } from "react-native-gesture-handler"
// export default function RegisterScreen({navigation}){

//     const onPressedSignup = ({navigation}) =>{
//         const emailError = emailValidator(email.value);
//         const passwordError = passwordValidator(password.value);
//         const nameError = nameValidator(name.value);
        
//         if(emailError){
//             setEmail({...email,error:emailError});
             
//         }
//     }
//     const [email,setEmail] = React.useState({value:"",error:""})
//     const [password,setPassword] = useState({value:"",error:""})
//     const [name,setName] = useState({value:'',error:''})

    
    
//     return(
//         <Background>
//             <BackButton goBack={navigation.goBack}/>
//             <Logo/>
//             <Text style={{fontSize:20,fontWeight:'700',paddingBottom:60}}>
//                 Create Your Account
//             </Text>
//             <TextInput
//                 value={name.value}
//                 error={name.error}
//                 errorText={name.error}
//                 onChangeText={(text)=> setName ({value:text,error:""})}
//                 label="Name"
//             />
//             <TextInput 
//                 label="Email"
//                 value={email.value}
//                 error={email.error}
//                 errorText={email.error}
//                 onChangeText={(text)=> setEmail({value:text,error:""})}
//             />
//             <TextInput 
//                 label="Phone Number"
//                 value={password.value}
//                 error={password.error}
//                 errorText={password.error}
//                 onChangeText={(text)=>setPassword({value:text,error:""})}
//                 secureTextEntry
//             />
//             <TextInput 
//                 label="Password"
//                 value={password.value}
//                 error={password.error}
//                 errorText={password.error}
//                 onChangeText={(text)=>setPassword({value:text,error:""})}
//                 secureTextEntry
//             />
//             <TextInput 
//                 label="Confirm Password"
//                 value={password.value}
//                 error={password.error}
//                 errorText={password.error}
//                 onChangeText={(text)=>setPassword({value:text,error:""})}
//                 secureTextEntry
//             />
//             <Button 
//             mode="contained"
//             onPress={onPressedSignup}
//             >Sign Up</Button>

//             <View style={styles.row}>
//                 <Text>Already have an account?</Text>
//                 <TouchableOpacity 
//                     onPress={()=>navigation.replace("LoginScreen")}
//                 >
//                     <Text style={styles.link}>Login</Text>
//                 </TouchableOpacity>

//             </View>


//         </Background>
        
//     )
// }

// const styles = StyleSheet.create({
//     container:{
//         flex:1,
//         backgroundColor:"#fff",
//         alignItems:"center",
//         justifyContent:"center",
//         width:"100%",
//     },
//     row:{
//         flexDirection:"row",
//         marginTop:4,

//     },
//     link:{
//         fontWeight:'bold',
//         color:theme.colors.primary,
//     },
// })

//api fetched below this/////

import React from "react"
import {View,StyleSheet,Text,Alert} from 'react-native'
import Header from "../components/Header"
import Paragraph from "../components/Paragraph"
import Button from "../components/Button"
import Background from "../components/Background"
import Logo from "../components/Logo"
import TextInput from "../components/TextInput"
import { useState } from "react"
import { emailValidator } from "../components/core/helper/emailValidator"
import { passwordValidator } from "../components/core/helper/passwordValidator"
import { nameValidator } from "../components/core/helper/nameValidator"
import BackButton from "../components/BackButton"
import { theme } from "../components/core/theme"
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler"

// ******************************************
const RegisterScreen = ({navigation}) => {
    
    const [CID, setCID] = React.useState("");
    const [first_name, setFname] = React.useState("");
    const [last_name, setLname] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [email, setEmail] = React.useState({value:"",error:""});
    const [p_number, setPNumber] = React.useState("");

  const addUser = () => {

    // navigation.navigate("LoginScreen")
 
    fetch(`https://doctorapplicaton.herokuapp.com/api/Patient/?`, {
      method: 'POST', // or 'PUT'
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ 
        cid: `${CID}`,
        first_name: `${first_name}`,
        last_name:` ${last_name}`,
        password:`${password}`,
        email: `${email}`,
        p_number:`${p_number}`
  
  
      }),
      
    })
    .then((response)=> {
        if (response.ok){
            navigation.navigate('LoginScreen')
        }
        else{
            Alert.alert('All fields are required')
        }
      })
    .catch((error) => {
        
      console.error('Error:', error);
    });
  }
  

    return(
        <ScrollView>

            <View style={styles.backgrnd}>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
            <Text style={{fontSize:20,fontWeight:'700',paddingBottom:0}}>
                Create Your Account
            </Text>
            <TextInput
                // value={name.value}
                // error={name.error}
                // errorText={name.error}
                // onChangeText={(text)=> setName ({value:text,error:""})}
                onChangeText={f_name => setFname(f_name)}
                label="FName"
            />
            <TextInput
                // value={name.value}
                // error={name.error}
                // errorText={name.error}
                // onChangeText={(text)=> setName ({value:text,error:""})}
                onChangeText={l_name => setLname(l_name)}
                label="LName"
            />
              <TextInput
                // value={name.value}
                // error={name.error}
                // errorText={name.error}
                // onChangeText={(text)=> setName ({value:text,error:""})}
                onChangeText={CID => setCID(CID)}
                label="CID"
            />
            <TextInput 
                label="Email"
                // value={email.value}
                // error={email.error}
                // errorText={email.error}
                // onChangeText={(text)=> setEmail({value:text,error:""})}
                onChangeText={email => setEmail(email)}
            />

            <TextInput 
                label="Phone Number"
                // value={password.value}
                // error={password.error}
                // errorText={password.error}
                // onChangeText={(text)=>setPassword({value:text,error:""})}
                onChangeText={p_number => setPNumber(p_number)}
                // secureTextEntry
            />
            <TextInput 
                label="Password"
                // value={password.value}
                // error={password.error}
                // errorText={password.error}
                onChangeText={password => setPassword(password)}
                // onChangeText={(text)=>setPassword({value:text,error:""})}
                secureTextEntry
            />
            {/* <TextInput 
                label="Confirm Password"
                // value={password.value}
                // error={password.error}
                // errorText={password.error}
                onChangeText={password => setPassword(password)}
                // onChangeText={(text)=>setPassword({value:text,error:""})}
                secureTextEntry
            /> */}
            <Button
            mode="contained"
            onPress={() => addUser()}
            >Sign Up</Button>

            <View style={styles.row}>
                <Text>Already have an account?</Text>
                <TouchableOpacity 
                    onPress={()=>navigation.replace("LoginScreen")}
                >
                    <Text style={styles.link}>Login</Text>
                </TouchableOpacity>

            </View>

            </View>


        </ScrollView>
        
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:"#fff",
        alignItems:"center",
        justifyContent:"center",
        width:"100%",
    },
    row:{
        flexDirection:"row",
        marginTop:4,

    },
    link:{
        fontWeight:'bold',
        color:theme.colors.primary,
    },
    backgrnd:{
        flex:1,
        padding:20,
        width:"100%",
        maxWidth:340,
        alignSelf:'center',
        alignItems:'center',
        justifyContent:"center",
    }
})

export default RegisterScreen;
