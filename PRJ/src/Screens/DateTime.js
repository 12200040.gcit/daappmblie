import React, {useState} from 'react'
import {StyleSheet,Text,View,TouchableOpacity,StatusBar} from 'react-native';
import {Calendar} from 'react-native-calendars';

const DateTime = () => {
    const [state, setState] = useState('');
    const onDayPress = (day) => {
        setState({
          selected: day.dateString
        });
        //navigation.navigate('Slot', { bookingDate : day })
      }
  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content"/>
      <View style={Commonstyle.toolbar}>
        <TouchableOpacity onPress={() => navigation.goBack() }><Text style={Commonstyle.toolbarButton}>Back</Text></TouchableOpacity>
                    {/* <Text style={Commonstyle.toolbarTitle}></Text>
                    <Text style={Commonstyle.toolbarButton}></Text> */}
      </View>
        <Calendar
          onDayPress={onDayPress}
          style={styles.calendar}
          hideExtraDays
          markedDates={{[state]: {selected: true}}}
          theme={{
            selectedDayBackgroundColor: 'green',
            todayTextColor: 'green',
            arrowColor: 'green',
          }}
        />
      </View>
  )
}

export default DateTime;
const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    calendar: {
      borderTopWidth: 1,
      paddingTop: 5,
      borderBottomWidth: 1,
      borderColor: '#eee',
      height: 350
    }
  });