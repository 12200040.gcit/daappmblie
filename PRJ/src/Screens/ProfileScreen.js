 
import React, {useState, useEffect} from 'react'

import { View,Text ,StyleSheet,ScrollView,TouchableOpacity,Image,FlatList,TouchableWithoutFeedback} from "react-native";
import { DataTable, Card, List,TextInput,Headline, Subheading, useTheme } from 'react-native-paper';


const ProfileScreen = ({navigation: { navigate }}) => {

    const [isLoading, setLoading] = useState(false);
    const [departments, setDepartments] = React.useState([]);

    const getDepartments = () => {
        fetch("http://doctorapplicaton.herokuapp.com/api/department/",
        {
          method: 'GET',
          headers: {
            accept: 'application/json',
          },
        })
          .then((response) => response.json())
          .then((json) => {
            setDepartments(json)
            })
          .catch((error) => console.error(error))
          .finally(() => {
            setLoading(false)
            console.log("Hello")
            }
          );      
      }
      useEffect(() => {
          setLoading(true);
          getDepartments();
      }, []);

  return (

    <View style={{flex:1}}>
          <Text style={{fontSize:20,fontWeight:'700',paddingHorizontal:20,paddingTop:10}}>
               Select a Department:
          </Text>
      <Card >
           <View style={{ padding: 20 }}>
            {isLoading ? <Text>Loading...</Text> :
            (
            
              <FlatList
                data={departments}
                keyExtractor={({dept_id}) => dept_id}
                renderItem={({ item }) =>(
                  <List.Item
                  title={item.dept_name}
                //   description = {item.dept_name}

                  style={{backgroundColor:'#4267B2',paddingVertical:39,marginTop:11,borderRadius:15}}
                  onPress={()=>navigate('Dentistrydoc',{item:item.emp_id})}

               
                  /> 
                )}
              />
              
            )}
          {/* <Button icon="camera" mode="contained" onPress={() => navigate("Add Item",{getUsers:getUsers})}>
            Add Item
          </Button> */}
        </View>            
      </Card>
      </View>
  )
}

export default ProfileScreen