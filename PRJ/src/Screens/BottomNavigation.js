import { StyleSheet, Text, View ,Image} from 'react-native'
import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from './HomeScreen';
import ScheduleScreen from './ProfileScreen';
import DoctorsScreen from './DoctorsScreen';

const Tab = createBottomTabNavigator();

const BottomNavigation = () => { 
  return (
          <Tab.Navigator   
          screenOptions={{headerShown:false}} >
            <Tab.Screen 
              name='Home'
              component={HomeScreen}
              options={{
                tabBarIcon:({size}) => {
                  return(
                    <Image 
                      style={{width:size,height:size}}
                      source={
                        require("../../assets/Home-icon.png")
                      } 
                    />
                  );
                },
              }}
            />
            <Tab.Screen 
              name='Schedule' 
              component={ScheduleScreen}
              options={{
                tabBarIcon:({size})=>{
                  return(
                    <Image
                      style={{width:size,height:size}}
                      source={
                        require("../../assets/Schedule.png")
                      }
                    />
                  );
                },
              }}
            />
            <Tab.Screen 
              name='Doctors' 
              component={DoctorsScreen}
              options={{
                tabBarIcon:({size})=>{
                  return(
                    <Image
                      style={{width:size,height:size}}
                      source={
                        require("../../assets/doc.png")
                      }
                    />
                  );
                },
              }}
            />
      
          </Tab.Navigator>
          
        )
      }

export default BottomNavigation

const styles = StyleSheet.create({})