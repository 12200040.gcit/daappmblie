import React from "react"
import {View,StyleSheet,Text} from 'react-native'
import Header from "../components/Header"
import Paragraph from "../components/Paragraph"
import Button from "../components/Button"
import Background from "../components/Background"
import Logo from "../components/Logo"

export default function StartScreen({navigation}){
    return(
        <Background style={styles.container}>
            <Logo></Logo>
            <Text style={{fontSize:20,fontWeight:'700',paddingBottom:80}}>
                DA App
            </Text>


            <Paragraph>Close to you</Paragraph>
            <Button 
            mode="contained"
            onPress={()=>{
                navigation.navigate("LoginScreen")
            }}
            
            >
                Login
            </Button>
            <Button 
            
            mode="contained"
            onPress={()=>{
                navigation.navigate("RegisterScreen")
            }}
            >Sign Up</Button>


        </Background>
        
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:"#fff",
        alignItems:"center",
        justifyContent:"center",
        width:"100%",
    },
})