

import React , { useEffect, useState }from "react";
import Header from "../components/Header";
import Background from "../components/Background";
import { View,Text ,StyleSheet,ScrollView,TouchableOpacity,Image,FlatList,TouchableWithoutFeedback} from "react-native";
import SearchBar from "../components/SearchBar";
import Category from "../components/Category";
import SelectDoctor from "./SelectDoctor";
import CategoryTwo from "../components/CategoryTwo";
import Button from "../components/Button";
import { theme } from "../components/core/theme";
import { DataTable, Card, List,TextInput,Headline, Subheading, useTheme } from 'react-native-paper';



export default function HomeScreen({navigation: { navigate }}){

    
    
    return(
        
        <View>
            
            {/* <SearchBar/> */}

            <Text style={{fontSize:20,fontWeight:'700',paddingHorizontal:20,paddingTop:20}}>
                Select Department:
            </Text>
            
            <ScrollView
                scrollEventThrottle={16}
            >
                <View style={{flex:1,paddingTop:20}}>
                    <Text style={{fontSize:20,fontWeight:'700',paddingHorizontal:20}}>
                        Departments
                     </Text>


                    <View>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}
                        >
                           
                            <View
                            // onPress={()=>navigation.navigate("Dentistrydoc")}
                            onPress={()=>navigate('Dentistrydoc')}
                            >
                            <Category 
                            imageUri={require('../../assets/Dod.png')} 
                            name="Department Of Dentistry"
                            />
                            </View>

                            <View
                            onPress={()=>navigate('Dentistrydoc')}
                            >
                          
                               <Category imageUri={require('../../assets/cardiologist.png')} 
                                name="Department of Cardiologiest"
                               />
                            </View>

                           
                            <View
                            onPress={()=>navigate('Dentistrydoc')}
                            >
                            
                              <Category imageUri={require('../../assets/Dos.png')} 
                              name="Department of Dermatalogy"
                               />
                            </View>
                            
                          <View
                          onPress={()=>navigate('Dentistrydoc')}
                          >
                          
                            <Category imageUri={require('../../assets/ENT.png')} 
                            name="Department of ENT"
                            />
                          </View>
                            
                            <View
                            onPress={()=>navigate('Dentistrydoc')}
                            >
                               
                              <Category imageUri={require('../../assets/orthopedic.png')} 
                              name="Department of Othopedic"
                              />
                            </View>
                            

                        </ScrollView>
                    </View>
                </View>

          {/* health care */}

             <View style={{height:405,marginTop:20}}> 
            <Text style={{fontSize:20,fontWeight:'700',paddingHorizontal:20}}>
                Health Care:
            </Text> 
            <ScrollView>
            
            <View style={{flex:1}}> 
                <View style={{flex:1,flexDirection:'row'}}>
                   <View style={{margin:15,backgroundColor:theme.colors.tint,borderWidth:2,borderColor:'white',borderRadius:10,padding:5}}>
                   <Image
                       source={require('../../assets/covid19.png')}
                        style={{width:150,height:150,resizeMode:"cover",}}
                       />
                    <Text style={{textAlign:'center'}}>Covid-19</Text> 
                   </View> 
                   <View style={{margin:15,backgroundColor:theme.colors.tint,borderWidth:2,borderColor:'white',borderRadius:10,padding:5}}>
                   <Image
                       source={require('../../assets/Nutrition.png')}
                        style={{width:150,height:150,resizeMode:"cover"}}
                       />
                    <Text style={{textAlign:'center'}}>Nutrition</Text>
                   </View>
                </View>

                <View style={{flex:1,flexDirection:'row'}}>
                   <View  style={{margin:15,backgroundColor:theme.colors.tint,borderWidth:2,borderColor:'white',borderRadius:10,padding:5}}> 
                   <Image
                       source={require('../../assets/fitness.png')}
                        style={{width:150,height:150,resizeMode:"cover"}}

                       />
                    <Text style={{textAlign:'center'}}>Fitness</Text>
                   </View> 
                   <View  style={{margin:10,backgroundColor:theme.colors.tint,borderWidth:2,borderColor:'white',borderRadius:10,padding:5}}>
                       <Image
                       source={require('../../assets/stroke.png')}
                        style={{width:150,height:150,resizeMode:"cover"}}

                       />
                       <Text style={{textAlign:'center'}}>Stroke</Text>
                   </View>
                </View>
            </View>
        </ScrollView>
        </View> 
                
        </ScrollView>
        </View>
        
      
    )
}
