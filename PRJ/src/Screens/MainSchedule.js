import React from "react";
import Header from "../components/Header";
import Background from "../components/Background";
import Button from "../components/Button";
import { View,Text ,StyleSheet,ScrollView,TouchableOpacity,Image,} from "react-native";
import SearchBar from "../components/SearchBar";




export default function ScheduleScreen({navigation}){
    return(
        
        <ScrollView>
            <SearchBar></SearchBar>
           
            <Text style={{fontSize:20,fontWeight:'700',paddingHorizontal:10,paddingTop:20}}>
                        Select Doctor to view their Schedule:
            </Text>


            <View style={styles.vieww}>
                    <View style={styles.view1}>
                        <Image 
                        style={styles.Dimg}
                        source={
                            require("../../assets/Dr.Lotay.png")
                        }/>    
                    </View>
                    <View style={styles.view2}>
                        <Text style={styles.dTitle}>Doctor Lotay</Text>
                        <Text style={styles.dDes}>Urologist at Ministry of Health, Bhutan {'\n'}</Text>  
                        <Text style={{color:'white',fontWeight:'bold'}}>9:00 am - 4:00 pm</Text>  
                                      
                    </View>
            </View>
            <View style={styles.vieww}>
                    <View style={styles.view1}>
                        <Image 
                        style={styles.Dimg}
                        source={
                            require("../../assets/doc1.png")
                        }/>    
                    </View>
                    <View style={styles.view2}>
                        <Text style={styles.dTitle}>Doctor James</Text>
                        <Text style={styles.dDes}>Carddiologist at Ministry of Health, {'\n'} Bhutan {'\n'}</Text> 
                        
                        <Text style={{color:'white',fontWeight:'bold'}}>9:00 am - 4:00 pm</Text>              
                    </View>
            </View>
            <View style={styles.vieww}>
                    <View style={styles.view1}>
                        <Image 
                        style={styles.Dimg}
                        source={
                            require("../../assets/doc1.png")
                        }/>    
                    </View>
                    <View style={styles.view2}>
                        <Text style={styles.dTitle}>Doctor Tandin</Text>
                        <Text style={styles.dDes}>Dermatologist at Ministry of Health,{'\n'}Bhutan{'\n'}</Text>  
                        <Text style={{color:'white',fontWeight:'bold'}}>9:00 am - 4:00 pm</Text>                 
                    </View>
            </View>
            <View style={styles.vieww}>
                    <View style={styles.view1}>
                        <Image 
                        style={styles.Dimg}
                        source={
                            require("../../assets/Dr.Lotay.png")
                        }/>    
                    </View>
                    <View style={styles.view2}>
                        <Text style={styles.dTitle}>Doctor Lotay</Text>
                        <Text style={styles.dDes}>Urologist at Ministry of Health, Bhutan {'\n'}</Text> 
                        <Text style={{color:'white',fontWeight:'bold'}}>9:00 am - 4:00 pm</Text>                 
                        
                    </View>
            </View>
            <View style={styles.vieww}>
                    <View style={styles.view1}>
                        <Image 
                        style={styles.Dimg}
                        source={
                            require("../../assets/Dr.Lotay.png")
                        }/>    
                    </View>
                    <View style={styles.view2}>
                        <Text style={styles.dTitle}>Doctor Lotay</Text>
                        <Text style={styles.dDes}>Urologist at Ministry of Health, Bhutan {'\n'}</Text>    
                        <Text style={{color:'white',fontWeight:'bold'}}>9:00 am - 4:00 pm</Text>              
                    </View>
            </View>
            <View style={styles.vieww}>
                    <View style={styles.view1}>
                        <Image 
                        style={styles.Dimg}
                        source={
                            require("../../assets/Dr.Lotay.png")
                        }/>    
                    </View>
                    <View style={styles.view2}>
                        <Text style={styles.dTitle}>Doctor Lotay</Text>
                        <Text style={styles.dDes}>Urologist at Ministry of Health, Bhutan {'\n'}</Text>    
                        <Text style={{color:'white',fontWeight:'bold'}}>9:00 am - 4:00 pm</Text>              
                    </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    dTitle:{
        fontWeight:'bold',
        fontSize:20,
        color:"#fff" 
    },
    dDes:{
        color:"#fff"
    },
    vieww:{
        display:'flex',
        borderColor:'black',
        flex:1,
        flexDirection:'row',
        backgroundColor:'#2e57ab',
        margin:5,
        borderRadius:10,
        
        
    },
    view1:{
        margin:5,
        
         
    },
    view2:{
        margin:5
        
        
    },
    Dimg:{
       width:100,
       height:100,
       borderRadius:20
        

    }
})
