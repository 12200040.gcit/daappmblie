import React from "react";
import Header from "../components/Header";
import Background from "../components/Background";
import { View,Text ,StyleSheet,ScrollView,TouchableOpacity,Image} from "react-native";
import SearchBar from "../components/SearchBar";
import Button from "../components/Button";



export default function AppointmentConfirmation(){
    return(
        <ScrollView>
            <SearchBar></SearchBar>
            <Header>Select Date and Time slot</Header>

            <View style={styles.vieww}>
                    <View style={styles.view1}>
                        <Image 
                        style={styles.Dimg}
                        source={
                            require("../../assets/Dr.Lotay.png")
                        }/>    
                    </View>
                    <View style={styles.view2}>
                        <Text style={styles.dTitle}>Doctor Lotay</Text>
                        <Text style={styles.dDes}>Urologist at Ministry of Health, Bhutan</Text>                 
                      
                    </View>
            </View>
            <View>
                <Header>Select Date:</Header>
            </View>
            <View>
                <Header>Select Time:</Header>
                
            </View>
            
            

        </ScrollView>
    )
}

const styles = StyleSheet.create({
    dTitle:{
        fontWeight:'bold',
        fontSize:20,
        color:"#fff" 
    },
    dDes:{
        color:"#fff"
    },
    vieww:{
        display:'flex',
        borderColor:'black',
        flex:1,
        flexDirection:'row',
        backgroundColor:'#80A3DE',
        margin:5,
        borderRadius:10,
        
        
    },
    view1:{
        margin:5,
        
         
    },
    view2:{
        margin:5
        
        
    },
    Dimg:{
       width:100,
       height:100,
       borderRadius:20
        

    }
})
// export default function DoctorsScreen({navigation}){
//     return(
        
//         <ScrollView >
//             <SearchBar/>
//             <Header>Select Doctors</Header>
//             <View style={styles.container}>
//                 <TouchableOpacity 
//                 style={styles.touch}
//                 onPress={()=>{
//                     navigation.navigate("ProfileScreen")
//                 }}
                
//                 >
//                     <View style={styles.first}>
//                         <Image
//                         style={styles.Dlogo}
//                         source={require('../../assets/Dod.png')}
//                         />                       
//                         <Text style={styles.white} >DEPARTMENT OF DENTISTRY</Text>
//                         <Button 
//                         mode="contained"
//                         >Book now</Button>

//                     </View>
                    
//                 </TouchableOpacity>
            
//                      <View style={styles.first}>
//                         <Image
//                         style={styles.Dlogo}
//                         source={require('../../assets/Dos.png')}
//                         />        
//                          <Text style={styles.white} >DEPARTMENT OF DERMATOLOGY</Text>
//                     </View>
//                     <View style={styles.first}>
//                         <Image
//                         style={styles.Dlogo}
//                         source={require('../../assets/ENT.png')}
//                         /> 
//                         <Text style={styles.white} >ENT</Text>
//                     </View> 
//                     <View style={styles.first}>
                   
//                         <Text style={styles.white} >ENT</Text>
//                     </View>
//                     <View style={styles.first}>
//                         <Text style={styles.white} >DENTISTRY</Text>
//                     </View>
//                     <View style={styles.first}>
//                         <Text style={styles.white} >DENTISTRY</Text> 
//                     </View>
//                     <View style={styles.first}>
//                         <Text style={styles.white} >DENTISTRY</Text> 
//                     </View><View style={styles.first}>
//                         <Text style={styles.white} >DENTISTRY</Text> 
//                     </View>
//             </View>
           
//         </ScrollView>
      
//     )
// }

// const styles=StyleSheet.create({
//     container:{
//         margin:5,
//         padding:10,
//         flex:1,
//         alignItems:'center',
//         justifyContent:'center'
        
//     },
//     white:{
//         color:'#fff'
//     },
//     first:{
//         backgroundColor:'#2e57ab',
//         width:'100%',
//         height:100,
//         margin:5,
//         alignItems:'center',
//         justifyContent:'center',
//         borderRadius:10,
        
        
      
//     },
//     touch:{
//         width:'100%',
//         flex:1,
//         alignItems:'center'
//     },
//     Dlogo:{
//         width:'40%',
//         height:'65%',
//         paddingLeft:'20%',
//         marginBottom:'2%',
//         borderRadius:5,
    
      
        
//     }
// })
