

import React, {useState, useEffect} from 'react'
import { View,Text ,StyleSheet,ScrollView,TouchableOpacity,Image,FlatList,TouchableWithoutFeedback} from "react-native";
import { DataTable, Card, List,TextInput,Headline, Subheading, useTheme } from 'react-native-paper';
import BackButton from '../components/BackButton';

const AppointmentList = ({navigation: { navigate }}) => {
    const [isLoading, setLoading] = useState(false);
    const [appointment, setAppointment] = React.useState([]);
    const getAppointment = () => {
        fetch("http://doctorapplicaton.herokuapp.com/api/appointment/",
        {
          method: 'GET',
          headers: {
            accept: 'application/json',
          },
        })
          .then((response) => response.json())
          .then((json) => {
            setAppointment(json)
            })
          .catch((error) => console.error(error))
          .finally(() => {
            setLoading(false)
            console.log("Hello")
            }
          );
      }
      useEffect(() => {
          setLoading(true);
          getAppointment();
      }, []);
  return (
      <Card 
      style={{}}   
      > 
          {/* <BackButton 
          style={{paddingLeft:20}}
          goBack={navigation.goBack}/> */}
        
           <View style={{ paddingTop: 60,paddingLeft:20}}>
            {isLoading ? <Text>Loading...</Text> :
            (
               
              <View>
             
                <Text style={{fontSize:20,fontWeight:'bold'}}>List of Appointment:</Text>
                
              <FlatList
                data={appointment}
                keyExtractor={({app_id}) => app_id}
                renderItem={({ item }) =>(
                  <List.Item
                  title= {item.app_date}
                  description = {item.app_time}
                  onPress={()=>navigate('DeleteScreen', {item:item, getAppointment:getAppointment})}
                  />
                )}
              />
               </View>
            )}
           

          {/* <Button icon="camera" mode="contained" onPress={() => navigate("Add Item",{getUsers:getUsers})}>
            Add Item
          </Button> */}
        </View>
      </Card>
  )
}
export default AppointmentList




// import React, {useState, useEffect} from 'react'
// import { View,Text ,StyleSheet,ScrollView,TouchableOpacity,Image,FlatList,TouchableWithoutFeedback} from "react-native";
// import { DataTable, Card, List,TextInput,Headline, Subheading, useTheme } from 'react-native-paper';
// import { AppointmentList } from '.';

// const AppointmentList = ({navigation: { navigate }}) => {
//     const [isLoading, setLoading] = useState(false);
//     const [appointment, setAppointment] = React.useState([]);
//     const getAppointment = () => {
//         fetch("http://doctorapplicaton.herokuapp.com/api/appointment/",
//         {
//           method: 'GET',
//           headers: {
//             accept: 'application/json',
//           },
//         })
//           .then((response) => response.json())
//           .then((json) => {
//             setAppointment(json)
//             })
//           .catch((error) => console.error(error))
//           .finally(() => {
//             setLoading(false)
//             console.log("Hello")
//             }
//           );
//       }
//       useEffect(() => {
//           setLoading(true);
//           getAppointment();
//       }, []);
//   return (
//       <Card>
//            <View style={{ padding: 20 }}>
//             {isLoading ? <Text>Loading...</Text> :
//             (
//               <FlatList
//                 data={appointment}
//                 keyExtractor={({app_id}) => app_id}
//                 renderItem={({ item }) =>(
//                   <List.Item
//                   title= {item.app_date}
//                   description = {item.app_time}
//                   onPress={()=>navigate('DeleteScreen', {item:item, getAppointment:getAppointment})}
//                   />
//                 )}
//               />
//             )}
//           {/* <Button icon="camera" mode="contained" onPress={() => navigate("Add Item",{getUsers:getUsers})}>
//             Add Item
//           </Button> */}
//         </View>
//       </Card>
//   )
// }
// export default AppointmentList