import React from "react"
import {View,StyleSheet,Text, Alert} from 'react-native'
import Header from "../components/Header"
import Paragraph from "../components/Paragraph"
import Button from "../components/Button"
import Background from "../components/Background"
import Logo from "../components/Logo"
import TextInput from "../components/TextInput"
import { useState,useEffect } from "react"
import { emailValidator } from "../components/core/helper/emailValidator"
import { passwordValidator } from "../components/core/helper/passwordValidator"
import BackButton from "../components/BackButton"
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler"
import { theme } from "../components/core/theme"


export default function LoginScreen({navigation}){
    // const onLoginPressed = () =>{
    //     const emailError = emailValidator(email.value);
    //     const passwordError = passwordValidator(password.value);
    //     if(emailError||passwordError){
    //         setEmail({...email,error:emailError});
    //         setPassword({...password,error:passwordError})       
    //     }
    //     else{
    //         navigation.navigate('HomeScreen')
    //     }
    // }
    const [login,setLogin]= React.useState();
    const [isLoading, setLoading] = useState(false);
    const [username,setUsername] = useState("")
    const [password,setPassword] = React.useState("")
    //    const [email,setEmail] = useState({value:"",error:""})
    // const [password,setPassword] = useState({value:"",error:""})


    const onLoginPressed =()=>{
        fetch ("http://doctorapplicaton.herokuapp.com/login/",
        {
            method: "POST",
            headers: {
              
                'Content-Type': 'application/json',
                // 'Accept': 'application/text' 
              }, 
            body:JSON.stringify({
                username:`${username}`,
                password:`${password}`

            })        
        })
    

        .then((response)=> {
            if (response.ok){
                navigation.navigate('HomeScreen')
            }
            else{
                Alert.alert('Incorrect username or password')
            }
          })
        // .done()
        .catch((error) =>{
            console.error('Error',error);})
        .finally(() => {
          setLoading(false)
          console.log("Hi")
          }
        );

   
        // const emailError = emailValidator(email.value);
        // const passwordError = passwordValidator(password.value);
        // if(emailError||passwordError){
        //     setEmail({...email,error:emailError});
        //     setPassword({...password,error:passwordError})       
        // }
        // else{
        //     navigation.navigate('HomeScreen')
        // } 

    }
    useEffect(() => {
        setLoading(false);
        // onLoginPressed();
      

  }, []);

 
    return(
       
        <ScrollView>

            <View style={styles.backgrnd}> 
           
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
            <Text style={{fontSize:20,fontWeight:'700',paddingBottom:80}}>
                DA App
            </Text>            
            <TextInput 
                label="Email"
                value={username}
                // error={email.error}
                // errorText={email.error}
                onChangeText={(username)=> setUsername(username)}

                // onChangeText={(email)=> setEmail({value:email,error:""})}


            />
            <TextInput 
                label="Password"
                value={password}
                // error={password.error}
                // errorText={password.error}
                onChangeText={(password)=>setPassword(password)}
                secureTextEntry
            />

            <Button 
            mode="contained"
            onPress={onLoginPressed}
            >Login</Button>



            <View style={styles.forgotPassword}>
                <TouchableOpacity 
                    onPress={()=>{
                        navigation.navigate("ResetPasswordScreen")
                    }}
                >
                <Text style={styles.link}>Forgot your password?</Text>
                </TouchableOpacity>

            </View>

            <View style={styles.row}>
                <Text style={{color:'black'}}>Don't have an account?</Text>
                <TouchableOpacity onPress={()=>navigation.replace("RegisterScreen")}>
                    <Text style={styles.link}>
                        Sign Up
                    </Text>
                </TouchableOpacity>
            </View>
  
            </View>
            </ScrollView>
            
   
        
        
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:"#fff",
        alignItems:"center",
        justifyContent:"center",
        width:"100%",
    },
    row:{
        flexDirection:'row',
        marginTop:4,
    },
    link:{
        fontWeight:'bold',
        color:'black',
    },
    forgotPassword:{
        width:'100%',
        alignItems:'flex-end',
        marginBottom:24,
    },
    forgot:{
        fontSize:13,
        color:theme.colors.secondary,
    },
    backgrnd:{
        flex:1,
        padding:20,
        width:"100%",
        maxWidth:340,
        alignSelf:'center',
        alignItems:'center',
        justifyContent:"center",
    }
})