import React , { useEffect, useState }from "react";
import Header from "../components/Header";
import Background from "../components/Background";
import { View,Text ,StyleSheet,ScrollView,TouchableOpacity,Image,FlatList,TouchableWithoutFeedback} from "react-native";
import SearchBar from "../components/SearchBar";
import Category from "../components/Category";
import SelectDoctor from "./SelectDoctor";
import CategoryTwo from "../components/CategoryTwo";
import Button from "../components/Button";
import { theme } from "../components/core/theme";
import { DataTable, Card, List,TextInput,Headline, Subheading, useTheme } from 'react-native-paper';



export default function SelectDate({navigation}){

    const [isLoading, setLoading] = useState(false);
  const [users, setUsers] = useState([]);
  //Load data from API
  const getUsers = () => {
    fetch("https://doctorapplicaton.herokuapp.com/api/appointment/",
    {
      method: 'GET',
      headers: {
        accept: 'application/json',
      },
    })
      .then((response) => response.json())
      .then((json) => {
        setUsers(json)
        })
      .catch((error) => console.error(error))
      .finally(() => {
        setLoading(false)
        console.log("Hello")
        }
      );      
  }
  useEffect(() => {
      setLoading(true);
      getUsers();
  }, []);
    
    return(
        
        <View>
            <SearchBar/>

            <Text style={{fontSize:20,fontWeight:'700',paddingHorizontal:20,paddingTop:10}}>
                Date
            </Text>
            
            <ScrollView
                scrollEventThrottle={16}
            >
                <View style={{flex:1,paddingTop:20}}>
                    <Text style={{fontSize:20,fontWeight:'700',paddingHorizontal:20}}>
                     select a time slot
                     </Text>

                    <View>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}
                        >



    <Card>
        <View style={{ padding: 20 }}>
            {isLoading ? <Text>Loading...</Text> :
            (
              <FlatList
                data={users}
                keyExtractor={({app_id}) => app_id}
                renderItem={({ item }) =>(
                  <List.Item
                  title={item.app_time}
                  left={props => <List.Icon {...props} icon="clock" />}
                  onPress={()=>navigation.navigate("SelectDate")}
                  /> 

                )}
              />
            )}
          {/* <Button icon="camera" mode="contained" onPress={() => navigate("Add Item",{getUsers:getUsers})}>
            Add Item
          </Button> */}
        </View>
      </Card>

                           
                            {/* <TouchableOpacity
                            onPress={()=>navigation.navigate("SelectDoctor")}
                            >
                            <Category 
                            imageUri={require('../../assets/Dod.png')} 
                            name="Department Of Dentistry"
                            />
                            </TouchableOpacity>

                            <Category imageUri={require('../../assets/cardiologist.png')} 
                            name="Department of Cardiologiest"
                            />

                            <Category imageUri={require('../../assets/Dos.png')} 
                            name="Department of Dermatalogy"
                            />

                            <Category imageUri={require('../../assets/ENT.png')} 
                            name="Department of ENT"
                            />
                            
                            <Category imageUri={require('../../assets/orthopedic.png')} 
                            name="Department of Othopedic"
                            /> */}

                        </ScrollView>
                    </View>
                </View>

         
        </ScrollView>
        </View>
        
      
    )
}


const styles=StyleSheet.create({
    container:{
        margin:5,
        padding:10,
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        
        
    },
    white:{
        color:'#fff'
    },
    first:{
        backgroundColor:'#2e57ab',
        width:'100%',
        height:100,
        margin:5,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:10,
        
        
      
    },
    touch:{
        width:'100%',
        flex:1,
        alignItems:'center'
    },
    Dlogo:{
        width:'40%',
        height:'65%',
        paddingLeft:'20%',
        marginBottom:'2%',
        borderRadius:5,
    
      
        
    },
    vieww:{
        display:'flex',
        borderColor:'black',
        flex:1,
        flexDirection:'row',
        backgroundColor:'#2e57ab',
        margin:5,
        borderRadius:10,
        marginLeft:10,
        marginRight:10
        
        
    },
    view1:{
        margin:5,
        
         
    },
    view2:{
        margin:5},

        dTitle:{
            fontWeight:'bold',
            fontSize:20,
            color:"#fff" 
        },
        dDes:{
            color:"#fff"
        },

        Dimg:{
            width:100,
            height:100,
            borderRadius:20
             
     
         }
})
