import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import DrawerContent  from '../DrawerContent'
import BottomNavigation from './BottomNavigation'
import { createDrawerNavigator } from '@react-navigation/drawer';
import SelectDoctor from './SelectDoctor';
import DateAndTime from './DateAndTime';
import HomeScreen from './HomeScreen';
import ProfileScreen from './ProfileScreen';
import DoctorsScreen from './DoctorsScreen';

const Drawer = createDrawerNavigator();


const DrawerNavigator = () => {
  return (
    <Drawer.Navigator drawerContent={DrawerContent}>
      <Drawer.Screen name='Welcome to DA!' component={BottomNavigation}/>
      {/* <Drawer.Screen name='SelectDoctor' component={SelectDoctor}/> */}
      <Drawer.Screen name='DateAndTime' component={DateAndTime}/>
      <Drawer.Screen name="HomeScreen1" component={HomeScreen}/>
      <Drawer.Screen name="ProfileScreen" component={ProfileScreen}/>
      <Drawer.Screen name="Do" component={DoctorsScreen}/>

    </Drawer.Navigator>
  )
}

export default DrawerNavigator

const styles = StyleSheet.create({})