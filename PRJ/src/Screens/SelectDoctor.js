import React from "react";
import Header from "../components/Header";
import Background from "../components/Background";
import { View,Text ,StyleSheet,ScrollView,TouchableOpacity,Image,StatusBar} from "react-native";
import SearchBar from "../components/SearchBar";
import Button from "../components/Button";
import BackButton from "../components/BackButton";
import DateAndTime from "./DateAndTime";

export default function SelectDoctor({navigation}){
    return(
        
        <View style={styles.WholeView}>

            <BackButton goBack={navigation.goBack}/>
            <Text style={{fontSize:20,fontWeight:'700',paddingHorizontal:10,paddingTop:10,paddingBottom:10}}>
                Select available Doctors
            </Text>
            <ScrollView>
            <View style={styles.vieww}>
                    <View style={styles.view1}>
                        <Image 
                        style={styles.Dimg}
                        source={
                            require("../../assets/doc1.png")
                        }/>    
                    </View>
                    <View style={styles.view2}>
                        <Text style={styles.dTitle}>Doctor Willie</Text>
                        <Text style={styles.dDes}>Dentist at Ministry of Health, Bhutan</Text>                 
                        <Button
                        onPress={()=>navigation.navigate("DateAndTime")}
                        mode="contained">Book Now</Button>
                        
                    </View>
            </View>
            
            
            <View style={styles.vieww}>
                    <View style={styles.view1}>
                        <Image 
                        style={styles.Dimg}
                        source={
                            require("../../assets/james.png")
                        }/>    
                    </View>
                    <View style={styles.view2}>
                        <Text style={styles.dTitle}>Doctor Brown</Text>
                        <Text style={styles.dDes}>Dentist at Ministry of Health, Bhutan</Text>                 
                        <Button mode="contained">Book Now</Button>
                    </View>
            </View>
        </ScrollView>
        </View>
        
    )
}

const styles = StyleSheet.create({
    dTitle:{
        fontWeight:'bold',
        fontSize:20,
        color:"#fff" 
    },
    dDes:{
        color:"#fff"
    },
    vieww:{
        display:'flex',
        borderColor:'black',
        flex:1,
        flexDirection:'row',
        backgroundColor:'#2e57ab',
        margin:5,
        borderRadius:10,

        
        
    },
    view1:{
        margin:5,
        
         
    },
    view2:{
        margin:5
        
        
    },
    Dimg:{
       width:100,
       height:100,
       borderRadius:20
        

    },
    WholeView:{
        paddingTop:60

    }
})
