import { DefaultTheme } from "react-native-paper";

export const theme = {
    ...DefaultTheme,
    colors:{
        ...DefaultTheme.colors,
        text: '#000000',
        primary:'#2C70EB',
        tint:'#FCFCFD',
        secondary:'#414757',
        border:'#C9DDEC',
        error:'#f13a59',
        sucess:'#00B386',
        google:'2E7D32',
        // Doc1:'#2e57ab',
        Doc1:"#2C70EB",
        Doc2:'#30E616',

    }
}