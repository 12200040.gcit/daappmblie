import React,{Component} from "react";
import {
    View,Text,StyleSheet,Image
} from "react-native";

class CategoryTwo extends Component{
    render(){
        return(
        <View style={{height:180,width:250,borderColor:'white',borderWidth:0.5,marginLeft:20,padding:10,backgroundColor:'blue'}}>
            <View style={{flex:2}}>
                <Image source={this.props.imageUri} 
                style={{flex:1,width:null,height:null,resizeMode:'cover',borderRadius:10}}
                /> 
            </View> 

            <View style={{flex:1,paddingLeft:10,paddingTop:10,}}>
                <Text style={{color:'white'}}>{this.props.name}</Text>
            </View>

            

          
        </View>
        );
    }
}
export default CategoryTwo;

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    }
})