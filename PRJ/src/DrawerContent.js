import React from "react"
import { View,StyleSheet} from "react-native"
import { DrawerItem,DrawerContentScrollView,Image} from "@react-navigation/drawer"
import {
    Avatar,
    Title,
    Caption,
    Paragraph,
    Drawer,
    Text,
    TouchableRipple,
    Switch,
}from 'react-native-paper'
import BottomNavigation from "./Screens/BottomNavigation"
import {MaterialIcons, MaterialCommunityIcons} from '@expo/vector-icons'
import { Fontisto } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';

export default function DrawerContent({navigation}){
    return(

        
        <DrawerContentScrollView>
            
           <View style={styles.drawerContent}>
               <View style={styles.userInfoSection}>
                   <Avatar.Image
                    size={80}
                    source={
                        require("../assets/profile.png")
                    }
                   />
                   <Title style={styles.title}>Chencho Tshering</Title>
                   <Caption style={styles.caption}>@chenchotsheri_</Caption>
                   {/* <View style={styles.row}>
                       <View style={styles.section}>
                           <Paragraph style={[styles.paragraph,styles.caption]}>
                               54
                           </Paragraph>
                           <Caption style={styles.caption}>Following</Caption>
                       </View>

                       <View style={styles.section}>
                           <Paragraph style={[styles.paragraph,styles.caption]}>
                               199
                           </Paragraph>
                           <Caption style={styles.caption}>Followers</Caption>
                       </View>
                   </View> */}
               </View>
               <Drawer.Section style={styles.drawerSection}>
               </Drawer.Section>
               <Drawer.Section style={styles.drawerSection}>
                   <DrawerItem
                    label="Appointment List"
                    onPress={()=>navigation.navigate("AppointmentList")}
                    icon={({})=><FontAwesome name="list-alt" size={24} color="black" />}
                   />
               </Drawer.Section>

               <Drawer.Section>
            
                   <DrawerItem
                    label="Home"
                    onPress={()=>navigation.navigate("HomeScreen")}
                    icon={({})=>< MaterialIcons name="home" size={24} color="black"/>}
                   />
                  
                   <DrawerItem
                    label="Schedule"
                    onPress={()=>navigation.navigate("ProfileScreen")}
                    icon={({})=><MaterialIcons name="schedule" size={24} color="black" />}
                   />
                   <DrawerItem
                    label="Doctors"
                    onPress={()=>navigation.navigate("DoctorsScreen")}
                    icon={({})=><Fontisto name="doctor" size={24} color="black" />}

              
                   />

               </Drawer.Section>
               <Drawer.Section style={styles.drawerSection}>
                   <DrawerItem
                    label="Logout"
                    onPress={()=>navigation.navigate("StartScreen")}
                    icon={({})=><MaterialIcons name="logout" size={24} color="black" />}
                   />
               </Drawer.Section>
               {/* <Drawer.Section style={styles.drawerSection}>
                   <DrawerItem
                    label="settings"
                    onPress={()=>{}}
                   />
               </Drawer.Section> */}
               {/* <Drawer.Section title="Preferences">
                   <TouchableRipple onPress={()=>{}}>
                       <View style={styles.preference}>
                           <Text>Notificatons</Text>
                           <View pointerEvents="none">
                               <Switch value={false}/>
                           </View>
                       </View>
                   </TouchableRipple>
               </Drawer.Section> */}
           </View>
        </DrawerContentScrollView>
    )
}

const styles = StyleSheet.create({
    drawerContent:{
        flex:1,
    },
    userInfoSection:{
        paddingLeft:20,
    },
    title:{
        marginTop:20,
        fontWeight:'bold',
    },
    caption:{
        fontSize:14,
        lineHeight:14,
    },
    row:{
        marginTop:20,
        flexDirection:'row',
        alignItems:'center',
    },
    section:{
        flexDirection:'row',
        alignItems:'center',
        marginRight:15,
    },
    paragraph:{
        fontWeight:'bold',
        marginRight:3,
    },
    drawerSection:{
        marginTop:15,
    },
    preference:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingVertical:12,
        paddingHorizontal:16,
    },
})